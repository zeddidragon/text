#!/usr/bin/env node

const fs = require('fs')
const showdown = require('showdown')
const converter = new showdown.Converter()

const dir = './edf/fic'
const files = fs
  .readdirSync(dir)
  .filter(name => name.slice(-3).toLowerCase() === '.md')

for(const file of files) {
  const path = `${dir}/${file}`
  fs.readFile(path, 'utf8', (err, md) => {
    if(err) return console.error(`Failed to read "${path}"`)
    const html = converter.makeHtml(md)
    const write = `${dir}/${file.slice(0, -3)}.html`
    fs.writeFile(write, html, (err, result) => {
      if(err) return console.error(`Failed to write "${write}"`)
      console.log(`Succesfully wrote "${write}"`)
    })
  })
}
