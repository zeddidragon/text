## Racoon
  - "Standard" class
  - Uses one-handed weapons, one hand reserved for parkour moves
  - SMG
  - Shotgun
  - Revolvers w/ scope
  - Sword
 
### Subweapons
  - Grenades
  - Attack drones
  - Mines
  - Grappling hook


All subwespons limited use but some more limited than others

## Walrus
  - Heavy class
  - Part robot, part walrus
  - Carries both weapons at once, jump button fires second weapon instead of jumping
  - Gatling lasers
  - Plasma launcher
  - Railgun
  - Barrier
 
### Subweapons
  - Shoulder mounted missiles
  - Jet roller skis
  - Personal Fusion Warhead
 
Warhead will take out user unless they shield. Care must be taken to prevent friendly fire.


## Sparrow
  - 