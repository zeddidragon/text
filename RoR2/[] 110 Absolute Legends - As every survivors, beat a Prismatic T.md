[*] 0/10 Absolute Legends - As every survivor, beat a Prismatic Trial without taking damage.  "Damage taken" stat should say 0 at the end. Teddy bears disqualify you as well.

[*] 0/10 Dance Like a Butterfly - As every survivor, beat the game using Glass as the only enabled Artifact. 

[*] "A'ight, I'mma Head Out" - On Scorched Acres, make a boss fall off the map. Cannot use Rex.

[*] Handicap - Defeat the spawns from hitting a 93% blood shrine while holding Defiant Gouge.

[*] Bled Dry - As Commando, kill Alloy Worship Unit in less than 30 seconds after he spawns.

[*] Thunderous Wrath - As Mercenary, kill Wandering Vagrant in less than 7 seconds without holding any items. (Probably can not be done on consoles).

[*] True Freedom - As Acrid, beat the game or obliterate without picking up items.

[*] Avengers, Assemble! - As Captain, kill a boss without any player characters' attacks or procs hitting it. Falling off the map also does not count. 

[*] Supplies! - As Captain, deal the final blow to a boss with a beacon