"What exactly are we supposed to rescue?" 

"Whatever you can. Don't come back with anything less."

Captain mused over his whiskey glass while watching his contractor leave the bar. Two ships had arrived at what star charts claim should be empty space, one of them carrying 7 billion units of high value cargo, and now it was his turn. Was he being sent on a scavenger hunt or to his death?

With not much to expect, the old man figured he'd have to get a bit of everything to staff The Good Travels.

---

"Your payment of TWEN-TY - DOLLAR CREDITS - has been deposited to your account. Happy hunting!"

Huntress had five more marks to get to tonight, but her prey was clutching something shiny, and she figured it might be worth taking a look at. She closed the distance in a blink and jump, and swiftly grabbed at the- nothing?

"Your speed beat mine for this kill, but I still won at real race." A man in blue armor stood by the side, tossing the jewel playfully in the air. He was fast. Huntress hadn't even seen him. Did he enter the scene and steal the jewel during her blink? That kind of speed shouldn't even be possible.

