## Numbers 

Dec  -> CW#  -> CW
0    -> o   -> Nil
1    -> -    -> Dot
2    -> /    -> Edge
3    -> v    -> Drop
4    -> #    -> Surf
5    -> x    -> Crack
6    -> y    -> Cut
7    -> m    -> Near
8    -> -o   -> Cube
9    -> --   -> Eye(s)
10   -> -/   -> Ebbo
11   -> -v   -> Drobbo
12   -> -#   -> Sublo
13   -> .x   -> Crablo
14   -> .y   -> Cublo
15   -> .m   -> Neblo
16   -> ⧉    -> Blocks
17   -> ⧉-   -> Dot and blocks
18   -> ⧉/   -> Edge and blocks
...
24   -> vo   -> Dropblocks
25   -> v-   -> Dot and dropblocks
...
32   -> #o  -> Surfblocks
40   -> xo   -> Crackblocks
48   -> yo   -> Cutblocks
52   -> mo   -> Nearblocks
64   -> -oo -> Tessaract
65   -> -o-  -> Dot and tess
66   -> -o/  -> Edge and tess
...
72   -> --o  -> Cube and tess
73   -> ---  -> Eye and tess
74   -> --/  -> Ebbo and tess
...
80   -> -⧉   -> Blocks and tess
81   -> -⧉-  -> Dot, blocks and tess
82   -> -⧉/  -> Edge, blocks and tess
...
100  -> -##  -> Surf, surfblocks and tess
1000 -> -mxo -> Crackblocks, Neartess, and supercube
4096 -> -oooo -> Hypercube
4097 -> -ooo- -> Dot and hyper


## Physical space

The unit in cubeworld so obvious that nobody named it. People simply say "Length of x", or "x length". Most people don't deal with volume, but the same goes. "Volume of x", but sometimes "Area of x-squared" or "X-cubed volume".

Still, some typical measurements:

Toss -> 48
Stroll -> 256

Plot -> 64 x 64
Acre -> 256 x 256

Cup -> 2 x 2 x 2
Bucket -> 3 x 3 x 5
Wagon -> 8 x 8 x 6

## Time

A day is 10(-/) phases. It starts at the crack of dawn when the sun pokes out just enough to light the east side of a cube, then the light gets a bit stronger and moves a bit south, lighting also the cube's front. It then rises to light the top,

| ------------| 0
| Dotlight    | 1
| Edgelight   | 2
| Dropping    | 3
| Full surface| 4
| Cracking    | 5
| Cutting     | 6
| Nearing dark| 7
| ------------| 8
                9

It's also common to just denote the time with the number. (Dot-time, edge-time...)

The hours between Nearing and Dotlight, when there's no sunlight, are collectively known as Darktime, but can still be distinguished numerically to Cube-time, Eye-time, and Nil-time. Cubeworld does not have a moon.

Cubeworlders typically sleep for 3 and a half phases, from Nearing to Dot.

Phases can be further split into tess-fractions, which is 1/64 of a phase. Tess-fractions ("Fracs", for short) can also be split into super tess-fractions, or "supes" in informal speech, which is further 1/64 of a tess-fraction.

## Days

| ------------|
| Topday      | (Top)
| Ledday      | (Left)
| Fronday     | (Front)
| Riday       | (Right)
| Backday       | (Back)
| Groundsday  | (Bottom)
| ------------|

Day calendar is a cube with markers on back and front. The front marker points up on topday. Groundsday is a day of rest.

## Seasons

| ------------|
| Chicken     | (Top-Left-Front)
| Plow        | (Top-Right-Front)
| Bovinestime | (Top-Right-Back)
| Hooves      | (Top-Left-Back)
| Eggsbatch   | (Bottom-Left-Front)
| Harvest     | (Bottom-Right-Front)
| Milkage     | (Bottom-Right-Back)
| Butchery    | (Bottom-Left-Back)
| ------------|

Calendars are cubes with a marker in corner. Combined day/season calendars involve a marker moved around on the day calendar, relative to the top.
