[h1]Gunships[/h1]
DE202 is a flying fortress equipped with a large variety of guns ranging in size from 105mm to 190mm. The effect of the guns vary greatly, and an Air Raider could very easily get away with equipping nothing but guns from this category and make very significant contributions to a team or hold up respectably solo.
Properties shared by gunship attacks include:
[list]
[*] Reloading over time in the background, even if you're in a vehicle.
[*] Aimed with a sticky beacon launcher.
[*] Relatively short delay.
[*] Mostly piercing shots.
[*] Attacks come in a line from somewhere in the sky.
[/list]

In EDF5 gunship strikes have transitioned from needing a small amount of credits to reloading by themselves over a short time, presumably to make them more appealing to a solo player. Almost not matter the situation, it's a good idea to bring a gunship strike in at least one slot due to their uptime, ease-of-use, and reliability.

Note that the beacon launches a sticky grenade that will attach to any enemies you hit and the strike's aim will follow them. The strike itself will be aimed directly at the enemy when fired, but it's not homing and can therefore still miss if the enemy moves between the shot being launched and hitting its mark. This property is very handy for continous attacks like 105mm and Autocannon, but less so for single-shot attacks like 150mm.

Another noteworthy feature of the beacons is that they'll disappear if whatever they're attached to disappears, and the strike starts reloading once the beacon is gun. If you place a 105mm on a building, for instance, and the strike itself destroys the building, the strike will start reloading at that moment even though the strike itself will run to completion. Putting the beacons on destructibles like fences and cars may therefore be to your advantage.

[h1]150mm Cannon[/h1]
[b]Damage[/b]:★★★★☆
[b]Speed[/b]: ★★★☆☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★☆☆☆☆
[b]Callin[/b]: Sticky Launcher
[b]Destructive[/b]

A staple of gunships, this calls in a heavy, piercing shot with blast radius from the sky. Damage is reasonably high and and normally enough to dispense with an enemy or a tight cluster of enemies in a single shot. New to EDF5 is the ammo capacity allowing you to fire multiple shots before the reload starts. Reload time is reasonably short.

Overall a solid, versatile strike for any ground-based enemies.

Recommended targets:[list]
[*]Species Alpha
[*]Generally useful
[/list]

[h1]Variant: 150mm Cannon Lapis[/h1]
[b]Damage[/b]:★★★☆☆
[b]Speed[/b]: ★★★★☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★☆☆☆☆
[b]Callin[/b]: Sticky Launcher
[b]Destructive[/b]

A variant of  150mm that fires faster. As a tradeoff it's not as powerful as regular 150mm, but you may  find it easier to hit with the delay  being shorter. Note that the projectile itself is not necessarily faster, so if you're attaching it directly to the enemy the normal variant may still have a higher chance of hitting.

Recommended targets:[list]
[*]Species Alpha
[*]Generally useful
[/list]

[h1]Variant: 150mm Triple/Quadruple-Barreled Cannon[/h1]
[b]Damage[/b]:★★★★★
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★★☆☆☆
[b]Callin[/b]: Sticky Launcher
[b]Destructive[/b]

A variant of  150mm that fires multiple shots in a burst. As the total damage of a burst is significantly higher, this is an excellent tool for punching above your weight. The increased amount of projectiles might also increase your chances of hitting, but each individual shot is also considerably less accurate.

Recommended targets:[list]
[*]Species Alpha
[*]Generally useful
[/list]

[h1]Autocannon / Minigun / Vulcan Cannon[/h1]
[b]Damage[/b]:★★★★☆
[b]Speed[/b]: ★★★☆☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★★☆☆☆
[b]Callin[/b]: Sticky Launcher

This is a solid, versatile line of strikes that rains heavy damage on your targeted area. If attached to an enemy, the strike will follow the enemy around as they move but may have trouble connecting if they move particularly fast. It can be beneficial to attach the strike to the ground and use it for area denial.

As the strike creates a column of deadly lead through the air, it makes for excellent anti-air when the air is thick enough with enemies. From hordes of small enemies to large enemies that need a lot of damage, there's virtually nowhere you can't take machinegun strikes.

The high damage is also practical for freeing yourself fairly safely from the grasp of monsters and webs.

Attaching a machine gun beacon to a humanoid will almost certainly kill them.

While reload time is extremely low, the strike itself lasts for a while, making it slow to reposition.

Recommended targets:[list]
[*]Flying-Types
[*]Drones
[*]Type-2 Drones
[*]Imperial Drones
[*]Deroys
[*]Humanoids
[*]Anchors
[*]Generally great
[*]Not overwhelmingly effective for regular Alpha and Beta aggressors.
[/list]


[h1]105mm Cannon Rapid-Fire Cannon[/h1]
[b]Damage[/b]:★★★★☆
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★★★☆☆
[b]Callin[/b]: Sticky Launcher
[b]Destructive[/b]

A terrifying area-denial tool, 105mm will destroy friends, foes, and buildings alike within a fairly large area. Generally good when placed in front of an advancing horde or just in front of a group of NPCs fighting an advancing horde.  Try to avoid including NPCs themselves.

Can be placed on a humanoid aliens to ruin any cover they try to run behind as well as generally wreck their day.

It's not recommended to attach these directly to monsters, as their flying corpses can send the strike in unpredictable directions.

Recommended targets:[list]
[*]Species Alpha
[*]Red Alpha
[*]Flying-Types
[*]Species Gamma
[*]Humanoids
[*]Anchors
[/list]

[h1]Rocket Cannon[/h1]
[b]Damage[/b]:★★★★☆
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★★☆☆☆
[b]Callin[/b]: Sticky Launcher
[b]Destructive[/b]

Rocket Cannon doesn't start with its best foot forward. Early version just seem like a weaker 150mm that sends enemies flying away. It's not until later in the line that it starts to shine with its superiour damage, reload times, and blast area.

This is a dangerous strike to use due to its large blast area and lack of peneatrative power and lower clip capacity, but the destructive potential is most certainly there.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Generally not bad
[/list]

[h1]120mm Neutralize Cannon[/h1]
[b]Damage[/b]:★★★☆☆
[b]Speed[/b]: ★★★★☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★☆☆☆☆
[b]Callin[/b]: Sticky Launcher

This is a sky shotgun. It calls a shotgun strike from the sky.

Clip capacity is quite high, and reload-time is not bad, so this is a versatile way to spread lead over many targets or pump it all into one target.

Recommended targets:[list]
[*]Flying-Type Aggressors
[*]Drones
[*]Type-2 Drones
[*]Imperial Drones
[*]Anchors
[*]Humanoids
[*]Generally useful
[/list]
