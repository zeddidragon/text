[h1]Stationary Weapons: Ordenance[/h1]
The name of this category is a bit misleading. Half the category consists of turrets, but the other half will not be standing still at all. A more fitting name might be "deployable weapons".

This section covers the deployable ordenance.

Many players will only give Stationary Weapons the time of day in the game's few underground missions, but the truth is that Stationary Weapons are incredibly powerful tools, and an Air Raider could probably blitz through the entire campaign without ever touching an air raid largely with tools from here.

Properties shared by deployable ordenance includes:
[list]
[*] Reloading manually over a short time. (Short for an Air Raider)
[*] Autonumous movement after deployment.
[*] Detonates in unorthodox manners, based on neither time nor contact.
[/list]

Despite the common traits of being deployed, self-moving bombs, the category maintains such an oddball collection of variations that it's hard to draw a more common thread than that.

Gone from this section is C-bombs and Y-mines, which Air Raider borrowed from Ranger in the previous game. He's more confident in this own tools this time, and has given them back in EDF5. Poor guy needed the burst damage.

[h1]Robot Bomb[/h1]
[b]Damage[/b]: ★★★☆☆
[b]Speed[/b]: ★★★☆☆
[b]Range[/b]: ★★★★☆
[b]Detonation[/b]: Target

Formerly known as Roller Bomb, these eplosive Roombas have gotten a dramatic revamp in the form of target-lock and pathfinding. To use it, simply look towards an until lock is achieved, then hold the fire button to deploy.

The potential is limited only by your imagination and patience, and a clever Air Raider can win entire battles without ever having to see an enemy by using these.

Your first instinct might be to bring it into underground missions, but since monsters like to climb on walls this will often lead to a lot of bombs stuck trying to pathfind and eventually harmlessly self-detonate. Instead, it works a lot better in urban areas, particularly against humanoids.

When stacked against a humanoid enemy, an air raider can lock on to them beyond any amount of cover, then send the army of bombs to remove their legs to open them for further attack, or simply send more bombs to finish the job. Sometimes the bombs will necessarily explode close to buildings, which might unwantedly remove cover. It remains an extremely effective weapon for the situation nontheless.

In sprawling underground missions, an Air Raider can look directly at the next group of enemies through any amount of terrain and send the bombs in advance to soften them up or pull aggro. Ropbot Bombs will last for around a minute before expiring, and can cover a pretty decent amount of ground while doing so.

Note that while Robot Bombs will always try to find the most efficient path possible, they will only consider strictly ground-based paths. If the enemy is on a cliff below you, for instance, Robot Bombs will opt to find the more wheelchair-accessible long way around to their targets.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Species Gamma
[*]Humanoids
[*]Compact Teleportation Devices
[/list]

[h1]Patroller[/h1]
[b]Damage[/b]: ★★★☆☆
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★★★☆☆
[b]Detination[/b]: Detector

This is essentially a mobile claymore mine. It will move dumbly forward and bounce off terrain. They're not easy to predict or control, but generally speaking sending them into a narrow tunnel will work out fine.

Projectiles from Patroller will pierce through enemies and bounce off surfaces, and are then capable of piercing through the same enemy once again, so damage potential is theoretically pretty high. Try sending them in a zig-zag for maximal bounces.

If you don't have a specific plan in mind, these generally make for a poor alternative ot Limpet Splendor.

Recommended targets:[list]
[*]Very narrow corridors
[/list]

[h1]Variant: Speed Star[/h1]
[b]Damage[/b]: ☆☆☆☆☆
[b]Speed[/b]: ★★★★★★★★★
[b]Range[/b]: ★★★★★
[b]Detonation[/b]: Harmless

This is just a fun toy for you to play around with. Every Air Raider is bound to jump with joy if they get these puppies instead of some other level 60 weapon dropped from their weapon crates!

Speed Stars will behave like Roller Bombs of old, and dumbly move forward until they bump into something that makes them turn around. In theory, it serves a purpose in letting you theorycraft Patroller paths quickly, but their immense speed could mean that their paths will differ from actual patroller by doing sweet jumps off ramps and generally bouncing a lot harder.

Have a Speed Star race with your friends!

Recommended targets:[list]
[*]Boredom
[/list]

[h1]Assault Beetle[/h1]
[b]Damage[/b]: ★★★★☆
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★★☆☆☆
[b]Detonation[/b]: Manual

In the previous game, Assault Beetle was quite an oddball without a specific purpose. Slower than Roller Bombs without being any meaningfully stronger, awkward to use, and not nearly as destructive as C-bombs, Assault Beetle suffered from falling between chairs.

EDF5 has hardly improved upon them at all. The deployment locks you into a highly risky throwing animation, which makes Assault Beetle unsuited as the close-quarters combat tool they could've otherwise served as.

They have an okay blast radius that makes them not entirely unsuited for underground traps, and can be stacked up on a shield bearer without invoking aggro, provided you can get within the shield without doing so in the first place.

Assault Beetles can jump pretty far, provided you're able to aim them right.

Recommended targets:[list]
[*]Shield Bearers
[*]Very narrow corridors
[/list]

[h1]Variant: Stag Beetle[/h1]
[b]Damage[/b]: ★★★★★
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★☆☆☆☆
[b]Detonation[/b]: Manual

Assault Beetles will usually go up and down, but Stag Beetle will simply go up.

If you're very close to a wall, you can attach Stag Beetle directly to it, and the clip capacity is simply ridiculous, allowing you to stack up over 80000 damage if you can attach all the beetles to a target. Try finding non-destructible environment to attach them to!

The beetles will inherit your own trajectory, so through running and jump you can give the beetle some forwards momentum.

The flying part might make it capable of hitting low-flying transport ships if you're very clever with them, and the damage output should make it go faster than a limpet sniper.

Recommended targets:[list]
[*]Shield Bearers
[*]Teleportation Ships, if low-flying and stationary.
[*]Very narrow corridors
[/list]

