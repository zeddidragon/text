[h1]Stationary Weapons: Turrets[/h1]
The name of this category is a bit misleading. Half the category consists of turrets, but the other half will not be standing still at all. A more fitting name might be "deployable weapons".

This section covers the deployable turrets.

Many players will only give Stationary Weapons the time of day in the game's few underground missions, but the truth is that Stationary Weapons are incredibly powerful tools, and an Air Raider could probably blitz through the entire campaign without ever touching an air raid largely with tools from here.

Properties shared by deployable turrets includes:
[list]
[*] Reloading automatically in the background, even in vehicles.
[*] The fire button throws an inert turret that will attach to vehicles, surfaces, and enemies but not players or NPCs.
[*] Air raider is locked into an animation while throwing them.
[*] Turrets are activated with the secondary effect button.
[*] Upon activation, turrets will fire until they run empty, then disappear.
[*] Turrets automatically track the nearest enemy, though some turrets track better than others.
[/list]

Due to the animation, turrets should not be thrown recklessly in mid-combat. The best time to deploy turrets is before the enemy is upon you, the second best is immediately after dodging their attacks.

[h1]ZE(R)-Gun[/h1]
[b]Damage[/b]: ★★★☆☆
[b]Range[/b]: ★★☆☆☆
[b]Tracking[/b]: ★★★★☆☆/★★★★★
[b]Friendly[/b]

ZE-Gun is the basic turret you start with, and they later evolve into ZER-Guns. The non-R variants fire fairly slowly and have poor tracking, but the enemies are slow enough at their level that it's usually fine.

The turrets are mostly good for smaller enemies, as larger enemies will have health pools exceeding their entire capacity, and constantly throwing bullets at humanoids is poor aggro management anyway.

ZER Guns are primarily good for two things: Holding back swarms at a chokepoint, and holding back swarms while kiting with a vehicle. Since the closest enemy will always be targeted, it will optimally push back incoming hordes to keep them a safe distance from you, and swiftly dispense with stragglers approaching you from the flanks.

All bullet-firing turrets are incapable of friendly fire, and bullets will pass cleanly harmlessly through friendlies. Empty vehicles will still be hit, however.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Flying-Types
[*]Tadpoles
[/list]

[h1]Variant: ZE-Sniper[/h1]
[b]Damage[/b]: ★☆☆☆☆
[b]Range[/b]: ★★★★★
[b]Tracking[/b]: ★☆☆☆☆
[b]Friendly[/b]

ZE-Sniper's poor tracking can be compensated for by using it's superlative range, but there's little commpensating for its poor total damage. It should do okay as mild support against flying types.

Recommended targets:[list]
[*]Easy Mode
[/list]

[h1]ZE-Launcher[/h1]
[b]Damage[/b]: ★★★☆☆
[b]Range[/b]: ★★★☆☆
[b]Tracking[/b]: ★★★☆☆

ZE-Launcher is a tricky one. With poor tracking and slow payload, these are unlikely to hit anything it specifically is aiming at. When swarmed by enemies, it can be assumed to simply fire rockets in random directions, and the risk of friendly fire goes up dramatically.

ZE-Launcher has the most effect when placed away from a thick group of enemies, and will thin the horde down as they approach. using the knockback to push them down a bit. It will also perform admirably when mounted on top of a vehicle circling a group of enemies from a distance.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Flying-Types
[*]Silver-Types
[/list]

[h1]FZ-Gun[/h1]
[b]Damage[/b]: ★★★★★
[b]Range[/b]: ★☆☆☆☆
[b]Tracking[/b]: ★★★★☆

This line of turrets shoots fire instead of bullets. The primary drawback of fire weapons is that they're risky because you need to be close to the enemy. As turrets are intangible and incapable of dying, this drawback is eliminated. FZ-Guns also notably last at least twice as long as other turrets. You can throw out FZ-Gun and ZE-Guns, activate them at the same time, and by the time your ZE-Guns have run out of ammo and reloaded, your FZ-Guns will still be firing.

Between the high damage output, piercing capabilities, wall-curving, and huge area covered, this is a top-tier Air Raider weapon with boundless applications.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Species Gamma
[*]Tadpoles
[*]Flying-Types
[*]Golden-Types
[*]Silver-Types
[/list]
