[h1]Artillery Strikes[/h1]
Artillery is a ground-to-ground strike launched using heavy cannons from afar. While not the fastest or the most accurate, all artillery strikes pack immense, indiscriminate firepower capable of changing the battlefield in a flash, provided you can hit with them.

Properties shared by artillery strikes include:
[list]
[*] Reloading using credits.
[*] Aimed with a smoke grenade.
[*] Incredible firepower.
[*] A delay of about 10 seconds between the grenade landing and the strike hitting.
[*] A quick, randomly scattered cascade of bombs spread over a large area.
[*] Building-destroying power and blast area.
[/list]

Many players shy away from Artillery Strikes due to the delay and their call-in tool being awkward to use, but there's an almost sure-fire way to hit your enemies: Drop the beacon at your feet. Enemies will come for you, so you know they'll gather inside the strike area. If you succesfully manage to exit the blast area in time, you'll find that a huge amount of your opponents will have evaporated afterwards.

Another way to use them is to bomb your area and simply not leave. Although artillery strikes are powerful, most vehicles should be able to survive staying within a small section of them, Barga in particular.

[h1]Mortar[/h1]
[b]Damage[/b]:★★★★★
[b]Speed[/b]: ★☆☆☆☆
[b]Range[/b]: ★★☆☆☆
[b]Area[/b] ★★☆☆☆
[b]Economy[/b]: ★★★★★
[b]Callin[/b]: Hand Grenade

New to EDF5 is the Mortar strike. They're usually at least as powerful as the next rung on the Howitzer ladder, and cost siginficantly less credits, but also covers a much smaller area. This means a lot more damage focused on a smaller point, but it retains all of the dalsy and difficulty of aiming that Howitzer has, and as such it can be very difficult to pack all of your enemies into the affected area.

Since the credit cost is so low, it can be used almost constantly. Consider this a high-level tool for experts.

You can perform just fine never using one, but it's also a good way to call powerful strikes without risking the lives of your teammates.

Recommended targets:[list]
[*]Type Alpha
[*]Humanoids
[/list]

[h1]Variant: Mortar Concentrated[/h1]
[h1]Mortar[/h1]
[b]Damage[/b]:★★★★★
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★★☆☆☆
[b]Area[/b] ★★★☆☆
[b]Economy[/b]: ★★★★☆
[b]Callin[/b]: Hand Grenade

This is a variant of Mortar that costs more but fires more shells over a longer duration. The extended duration should make timing more lenient, and can be used as an area denial tool. The expense is higher, but this is still a pretty cheap strike.

Recommended targets:[list]
[*]Type Alpha
[*]Humanoids
[/list]

[h1]Howitzer[/h1]
[b]Damage[/b]:★★★☆☆
[b]Speed[/b]: ★★☆☆☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★★★★★
[b]Economy[/b]: ★★☆☆☆
[b]Callin[/b]: Hand Grenade

This is Air Raider's signature strike. While slow, it's capable of leveling a neighbourhood in a single, humongous burst. Only one other weapon in the game is capable of matching the sheer area covered by Howitzer.

The damage is generally enough to weaken or kill most giant insects, depending on how many bombs hits them, so it's a good strike to call in on groups of weaker enemies.

Howitzer is powerful, but its cost is high, and the strike takes a long time to hit after being called, so use wisely.

Recommended targets:[list]
[*]Entire city blocks
[*]Type Alpha
[*]Type Beta
[*]Flying Types
[*]Tadpoles
[/list]

[h1]Cannon Gun[/h1]
[b]Damage[/b]:★★★★★★★★★★
[b]Speed[/b]: ★☆☆☆☆
[b]Range[/b]: ★★★☆☆
[b]Area[/b] ★★☆☆☆
[b]Economy[/b]: ★★☆☆☆
[b]Callin[/b]: Hand Grenade

No single attack in the game can match the damage inflicted by Cannon Artillery Strike. The final version hits a relatively small area (for an airstrike) with 20 shots of nearly 60 000 damage, for a total of 1.2 million damage. You should not bring for the purposes of bombing your own vehicle.

While almost nothing can possibly survive a full barrage from Cannon, the blast radius of the piercing projectiles is rather small,  and the strike is slow to come in, so it's possible to miss even humongous targets, as they will have plenty of time to get out of the way.

Note that Cannon Strike starts mostly with empty credits, and cannot be used at the start of a mission. Additionally, it's fairly expensive to refill. However, its high damage means that when used on targets that require it, Cannon Strike is capable of paying for its own upkeep.

Recommended targets:[list]
[*]Silver Types
[*]Humanoids
[*]Anchors
[*]Hives
[*]Boss Monsters
[*]Aranea Webs
[/list]
