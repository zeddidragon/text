[h1]Bombers[/h1]
Air Raider commands an array of bomber plane squads with a large variety of payloads. These are generally favoured by players for their large destructive potential, ease of use, and credit efficiency.

Properties shared by bombing plans include:
[list]
[*] Reloading using credits.
[*] Precisely aimed using an overhead tactical view, including direction.
[*] Reasonable delay.
[*] Enormous coverage.
[*] Attacks come from planes physically entering the combat area.
[/list]

The overheaed view is new change to EDF5, and a quite welcome one. Just by bringing a strike you can hold down the attack button to get a view of the battlefield, exiting without attacking with a weapon switch. It's also possible to quickly the call in the strike by simply tapping the attack button if you just want the strike somewhere in front of you.

Note that you cannot move while aiming the strike, and as such you're rendered vulnerable to enemy attacks.

[h1]Combat Bomber KM6[/h1]
[b]Damage[/b]:★★★★☆
[b]Speed[/b]: ★★★★☆
[b]Area[/b] ★★★☆☆
[b]Economy[/b]: ★★★★★
[b]Callin[/b]: Tactical View

KM6 is loaded with heavy machine guns, and a call will have it unleash hundreds of bullets over the battlefield. Each call in this line is distinct, ranging from high-speed variants that come and go at incredibly speeds, to an array of 9 planes that come in from each direction. Be sure not to dismiss any earlier KM6 strikes based on level alone, see what makes it unique and consider if that particular strike is what you need for a mission.

Due to the strike cutting a solid line through the air, KM6 makes for superlative anti-air. The low credit cost means you can bring it almost anywhere thick with enemies.

Recommended targets:[list]
[*]Flying-Types
[*]Tadpoles
[*]Drones
[*]Type-2 Drones
[*]Hives
[*]Generally very effective
[/list]

[h1]Scout Bomber / Heavy Bomber Phobos[/h1]
[b]Damage[/b]:★★★★☆
[b]Speed[/b]: ★★★☆☆
[b]Area[/b] ★★★☆☆
[b]Economy[/b]: ★★★★☆
[b]Callin[/b]: Tactical View

This is likely what comes to your mind when you heard the word "Bombers". One plane sails over the map and drop bombs in an immense area. It's a bit slow to call in, but the destructive power is well worth it.

The majority of Phobos strikes are only a single plane with an incredibly powerful payload. For Phobos 3 and 4, see the relevant section.

Recommended targets:[list]
[*]Species Alpha
[*]Species Gamma
[/list]

[h1]Variant: Heavy Bomber Phobos Plan 3/4[/h1]
[b]Damage[/b]:★★★☆☆
[b]Speed[/b]: ★★★☆☆
[b]Area[/b] ★★★★★
[b]Economy[/b]: ★★★☆☆
[b]Callin[/b]: Tactical View

This is likely what comes to your mind when you heard the word "Bombers". Multiple planes sail over the map and drop bombs in an immense area. It's a bit slow to call in, but the destructive power is well worth it, and the area covered is so large that it's hard not to hit.

Do not to use this strike recklessly. In particular it's not the best idea to bring it for missions with aliens as it'll destroy a whole lot of valuable cover and pull more aggro than necessary. The area covered is also large enough that teammates may not know how to avoid it in time if they're engaged in combat at the time.

Effective use of Phobos Plan 4 to immediately wipe out every wave of enemies is a good way to make your teammates bored and leave.

It is possible to survive within a Phobos multi plan by standing between the lines of bombing. Pay attention to the map for these safe zones.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Species Gamma
[/list]

[h1]Variant: Heavy Bomber Phobos Cluster Bombs[/h1]
[b]Damage[/b]:★★★☆☆
[b]Speed[/b]: ★★★☆☆
[b]Area[/b] ★★★★★
[b]Economy[/b]: ★★★☆☆
[b]Callin[/b]: Tactical View

#TODO: I haven't used these

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Species Gamma
[/list]

[h1]Combat Bomber Kamuy[/h1]
[b]Damage[/b]:★★★★☆
[b]Speed[/b]: ★★★★☆
[b]Area[/b] ★★☆☆☆
[b]Economy[/b]: ★★★★☆
[b]Callin[/b]: Tactical View

Unlike Phobos, Kamuy is only ever one plane, and the explosions from its paylad is smaller in size or weaker. The plane will respond quickly to your request, however, and start bombing almost immediately. The strike is also somewhat cheaper.

Because of its ease of use and low cost, you can likely call Kamuy many times for each time you would use Phobos. If you can direct it efficiently, Kamuy is a surgical precision tool that will serve you well.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Species Gamma
[*]Humanoids
[/list]

[h1]Heavy Bomber Vesta[/h1]
[b]Damage[/b]:★★★☆☆
[b]Speed[/b]: ★★★★☆
[b]Area[/b] ★★★☆☆
[b]Economy[/b]: ★★★★★
[b]Callin[/b]: Tactical View
[b]Non-Destructive[/b]

Vesta has a unique payload in that it drops napalms. The napalms are fickle, and will attach whatever surface it lands on. If this is a building, and the building disappears, the fire will as well. If this is an enemy, the enemy will yank the fire away with it when it does.

For this reason, Vesta should be used somewhere open and without obstacles, and must be called ahead of time before the enemy reaches. When performed correctly, you will have created a firewall that will last for upwards to a full minute. Most monsters will be unaware that they can simply walk around it, and as such this is a good crowd-control option.

Credit cost is low enough that Vesta can most likely be called again as soon as it expires.

Recommended targets:[list]
[*]Species Alpha
[*]Species Beta
[*]Species Gamma
[*]Humanoids
[/list]
