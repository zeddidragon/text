# You're Supposed To Be Trained

  >  **AF14**
  >  --------------------------------------
  >  Capacity: 120
  >  ROF: 12.0/sec
  >  Damage: 10
  >  Reload Time: 1.5sec
  >  Range: 150.0m
  >  Accuracy: B
  >  --------------------------------------
  >  A high-powered, fully automatic rifle. Developed in 2011, this new model boasts considerably higher firepower than the original. It has now been adopted as the EDF standard. In eight years of battling massive aliens on the field, it has delivered continuously strong results.

---

## June 4th, 2025

Downtown, EDF had moved on to another batch of insects chasing civilians. The operation had so far lasted for 4 hours, and there appeared to be no end to new reports of attacks coming in. Captain Arata of Storm team had run low on grenades and was getting closer up to the actions with his AF14 assault rifle.

"Ah, yes. I'm the ravager researcher, O'hara." A new voice crackled over the comm network. "I'd like to offer my advice to the soldiers. After examining dead giant insects, I found something surprising! After seven years, the giant insects have evolved, and grown stronger! They're covered by a stronger shell, and cannot be killed easily."

Arata could barely make out a second, female voice in the background of the radios message. "Now I don't have your fancy college degree, but shouldn't you be researching a bit longer than five seconds before- *crackle*" 

Without the distance and pure firepower of the new grenades, Arata could tell there was something to the man's words. The giant ants were taking a lot more bullets than he was used to them doing.

"But, it is not a problem. EDF's equipment has also grown stronger in seven years. You can definitely beat the evolved giant insects! I wish you good luck in battle!"

Almost as if on cue, another horde of ants encroached over the plaza up the street. Arata and his accompanying rangers opened fire at a full force. Ants were going down, but not fast enough. The mass of black encroached ever closer, clambering over their fallen allies.

"This is Ranger-16!" Ranger-16's radioman spoke up. "The data is incorrect! The insects are stronger and more violent than every before! We can't do this by ourselves!"

"Ranger-16!" HQ snapped back. "You're supposed to be trained in fighting giant insects! Hold your ground!"

A ranger looked around in panic, before deciding to run off into an alleyway. Almost as soon as he got in, he came shooting back out, covered in orange, viscous liquid. His armour was eroding, and it was already possible to see bone emerging where his face used to be.

"They're coming from behind us! It's a trap!" A member of Ranger-2 pointed back at the street they'd approached from.

"We just came from there. How are there so many?"

There wasn't any time to wonder. With the ants already being too close for grenades, Arata had no choice but to run for cover under a raised pedestrian tunnel network and spray lead in all directions. Multiple ranger teams followed suit, but not all made it. Men were eaten on the spot, or left to dissolve painfully in wads of acid.

"This is Ranger-2! We're fighting giant insects. Requesting wing diver support!"

"This is Ranger-16! They're picking up people like ragdolls! WUAGGH!" Ranger-16 got picked up like a ragdoll. The ant shook him around to prevent him from fighting back, but was promptly met with a hail of machinegun fire through the eyes.

Ranger-16 flopped to the ground, and scrambled to his feet. "What sharp pinchers!", he exclaimed. The man's armour had been cracked, and its owner was bleeding profusely out the side.

"This is Ranger-6! Send wing diver support!"

HQ snapped back. "It will take time for wing divers to arrive. Hold your ground! You need to protect the citizens!"

Ranger-6 cursed. "Looks like we're on our own for now. But there's no way we can hold back all of these bugs!"

Arata didn't answer, he was stepping backwards slowly, but with purpose, carefully calculating where his machinegun fire would make the biggest difference in preventing men from getting snatched. With his off-hand, he was pulling Ranger-16 out of the fray.

"Trained in fighting giant insects." Ranger-16 muttered. "What a joke." He coughed up blood in a mock laugh.

"Stop talking, save your strength." Arata grumbled during a reload.

"You should know!" Ranger-16 snapped back. "Maybe a dozen soldiers survived the previous war. Almost noone here has even SEEN a giant insect."

The ranger threw off Arata's grasp, and went running out towards the insects. Only too late did Arata notice he was one grenade lighter.

"Over here, space bugs! Come get your free lunch!"

A sizable portion of the ants turned around and went for the easier meal presented to them. Arata had seen this scenario too many times, and knew it wouldn't be the last. Thinking quickly, he beckoned for the remaining rangers to follow him into the subways where they could hide better. Behind him, agonized screams and chewing noises were quickly cut short by a loud explosion. Promptly after, the entrance collapsed, covering their retreat.

---

A kilometer up in the air, ravager researcher O'hara was watching the losing battle from the safety of an S09 Brute.

"Are the wing divers here yet?" He asked, worriedly.

"Doc, the comms are still on." Porters let him know. "Your fretting is probably not great for morale."

O'hara continued as if he meant to broadcast that all along. "No matter how much the giant insects evolved, they can't fly; so we can destroy them with an air attack! If the wing divers take them down, we can win!"
