# Story beats to hit
- Ranger-8 members survives the 2018 attack on the mothership, gets mistaken for Storm 1.

- The year 2025 ants attack again. O'hara talks about how fine they'll all do.

- EDF gets swarmed, but Wing Divers appear to save the day.

- Storm-1 is pleased with how well he worked together, requests to have Wing Diver appointed to Storm Team.

- Wing Diver is not impressed by legendary Storm-1's abilities.

- Wing Diver gets snatched by Retarius, saved by Storm-1, respect gets earned.

- Storm Team gets swarmed by spiders, is saved by an engineer that is next member of storm team appointed by HQ.

- Chapter or two of funny war stories.

- Storm joins a group of fencers attacking shield bearers. A fencer of few words kicks obscene amounts of ass, recruited for Storm team.

- Some chapters of funny war stories. Wing Diver is taken to Fencer, but gets shown no interest back.

- Operation Brute Force.

- Operation to take out queen. Storm team gets sepereated by darkness, confusing layout, and intense battles. AR gets himself surrounded. Ranger falls of a bridge and finds himself trapped in a mortar-like cave. Fencer and WD takes out ant queen together.

- Wing Diver eventually leaves team to lead Pale Team, an elite unit using new thrusters that can fly without limit. Pale Team goes onto achieve many amazing feats.

- Fencer gets injured. Storm-1 finds there to be a horribly disfigured man underneath their armour, and learns him to be the real Storm-1 from the previous war. Air Raider confesses to be Storm-2, but would rather stay in the shadow. Storm-1 assures them both that they've already more than lived up to their legends taking part of Storm Team.

- Ranger and AR takes down the mothership, this time with much more ease. Fencer is proud.

- Pale Wing falls victim to wasps. Fencer rushes off to find survivor among them. AR helps look in his Caravan.

- Ranger operates Balam and prevails against Erginus.

- WD is found, needs time to recover.

- Ranger and AR take out Quad Fortress.

- Fencer cuts down hive using lasers, knocks it over with hammers.

- WD and Ranger enters the hive, takes out the queen.

- Fencer and AR joins Battle of Giants.

- WD flies off to a different continent to assault the last remaining mothership, gets caught in the explosion.

- The trio joins to take out the Brain O'hara died to find. As it escapes, Fencer learns of WD's fate and becomes enraged.

- War stories of Fencer taking out impossible odds by himself.

- Ranger fights GWD.

- Pale Wing is found. Takes on Massive Mobilization. Everyone dies except her. At this experience, she understands what Storm team went through in the previous war.

- AR fights Argo.

- A massive army befalls Ranger and AR. Fencer arrives and kills everything with ridiculous ease.

- Battle with Brain:
    - Ranger shoots down plate. Ranger and Fencer shoot at brain. New plates appear.
    - Everyone takes cover, bemoans lack of window to fire back. Air Raider calls satellite blaster from underneath brain.
    - New cubes spawn in. Storm Team gets hurt, out of ammo.
    - WD arrives when all seems hopeless and fires a Gungnir. Earth is saved.
