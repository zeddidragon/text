XX

Captain Arata of Storm Team found himself speechless. Before him stood the most unassailable stronghold he'd ever bore witness to, and that's including the two motherships he'd taken down so far. 

This city was entirely covered in webs. Large webs, small webs, enormous webs, city-spanning webs. On the webs were Retarius, but but not just any old Retarius either. These had mutated, and seemed purple in color. Arata could tell that these must surely be able to shoot further than the regular variant, just judging from the webs they made. 

Underneath all the sheer webbing he could just faintly make out burrows. They seemed to be emerging from the sides of buildings, such was the extent of how infested this city was. Guarding the burrows were king spiders and ant queens, which were in turn guarded by the previously observed webs and mutant retarius. 

Arata had to think about this one for a while. He looked over at Siegfried for advice, but the team's Air Raider was pointing his phone at the city, taking pictures. 

"They haven't spotted us yet", he started. "Daitan, why don't you-" 

"Forget about it." 

"Fair. You'd probably get sniped out of the air the second combat breaks out."

Arata pondered some more, giving Siegfried another glance. The Raider was talking excitedly on the phone. He was standing a bit far away, but Arata could make out the phrase "Are you seeing this shit?" 

"Then Mataru, maybe you could take point and-" 

"Boss, I jumped off a helicopter onto a mothership and blew it up before leaving it, but even I'm not this suicidal."

"What should we do then? We can't just leave this city."

The Captain once again looked over at Storm-2. The Raider had taken to relaxing on a park bench. He appeared to be browsing his phone. Arata could just barely make out some kind of elevator music coming from the phone. Was he on hold with customer service?

"We need to find a weak point", Arata reasoned. "Fencer, Diver, I'd like you to circle around the city and see if there's anywhere in their formation we could wedge in or pick any enemies off without getting them on alert. At this point, I'll take even the smallest crack in their defense."

The two strikers nodded, then disappeared in jets of plasma and fire. It was little more than passing time, they all knew that, but maybe they'd be able to work up some courage through sheer restlessness in that time.

Arata propped up his Lysander at a park bench of his own and started surveiling from his scope.

---

The sun was well into setting, four hours later, and it was getting dark. Neither from the air, ground, or through sniper scope had any of Storm Team fount any weak spots. No matter how they approached, they were certain that every last bug in the city would react and overwhelm them.

With all the webs covering the top, even air raids seemed futile. Maybe Siegfried had already concluded as much and that's why he'd given up so early, at least that's what Arata could reason.

Couldn't hurt to ask, though.

Arata slumped onto the park bench next to Siegried, who was sprawled out in a very bored position, staring straight up at the sky. He appeared to have headphones plugged in.

"Siegfried, I could really use your input on this-"

"I had to call in a lot of favours for this one." Siegfried responded.

"Excuse me?"

"I thought it'd be enough to just show HQ how utterly lost this city is, but I ended up having to climb the chain of command up and down until I could find someone with the balls to approve it."

"Approve what?"

"I gave up in the end, and ended up just asking Despina's Captain directly about it. Turns out he had exactly two stockpiled from that operation way back in June last year."

"In June?"

Gears churned in Arata's head, but then it dawned on him, much like the actual sunrise spreading over him at the moment.

It then simultaneously dawned on him that it was currently dusk, and the sun wasn't even done setting yet. What came glowing over the horizon wasn't the sun at all.

Siegfried stood up, then started aiming his laser pointer with purpose. "Just in time, Duncan!" 

"Same-day shipping as promised, Air Raider!", the Captain jest back over comms.

Arata could only gape. "Those are-"

---
  > **Tempest S1A**
  > Damage: 100000
  > Homing Capability: B
  > Blast Area: Radius 100m
  > Reload: 6400pt
  > Shots: 1
  >  --------------------------------------
  > A Laser Guide Kit that transmits coordinates to battleship Despina to launch a Tempest S1A supersonic ballistic missile. This laser lock must be maintained until the missile hits, which can make participating in combat fairly challenging. However, the target location can be changed on the fly by moving the laser, granting some degree of control over the missile. This Laser Guide Kit is also equipped with a scope for ease of use.

---

"The Tempests used to shoot down the incoming motherships when this invasion first started. The operation was aborted while two missiles were left unfired."

Siegfried aimed one of the missiles a bit to the side, and it curved after with unnatural dexterity for a missile as large as the skyscraper it was barreling against. The glow from the two missiles enveloped the entire city and its outskirts in a light as bright as the sun itself. 

"Duncan had been carrying them around ever since. He said he saved them for just such an occasion."

The laser pointer flitted back and forth, with Siegfried frantically turning a dial to alternate between the two missiles, carefully micromanaging them.

The insects took note. Retarius from the complete other end of the city sent webs into the missiles, but it was far too little, far too late. Mutated or not, each missile had the size and mass of a cruise ship, and they wouldn't budge for anything. Not that strength mattered anyway, as the webs scorched from merely touching the missile. 

Missile #1 snuck around and between webs, then hit into the side of the tallest building, burying its way in by sheer force. It detonated, bringing the entire city center down with it. Webs were torn down or simply scorched away, and hundreds of giant insects shrieked out in agony for only a split second before being vaporized. Storm Team were blasted backwards from shockwaves as rubble, furniture, cars, entire floors of building went hurling over their heads. Arata could just barely make out the limpless, scorched corpse of a retarius sailing by, contorted into a gesture that reminded him of a defeated shrug.

After getting his breath back, then coughing up dust, then getting his breath back again, Arata strained to get up and surveyed the aftermath. Every tree in the park they were in had been uprooted and lay pointing away from the city center, every last leaf burnt away. Arata made note of thanking the armour development team back home for their impressive work in keeping him alive this mission. 

In the city, only a few figures remained. Spider kings, ant queens. They stared at each other briefly in confusion, then looked clear at Storm Team. One of the ant queens shrieked out a battle cry to order the surviving behemoths to charge, then got flattened under the mass of missile #2. A second fireball erupted from the flattened queen, taking out any remaining life within the city ruins.

Just like that, in the span of ten seconds, the city had turned from being unassailable to being dust. 

"I owe that man the highest of fives.", Siegfried mused. 
